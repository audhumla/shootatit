import javax.swing.*;
import java.lang.Object;
import java.awt.FlowLayout;


public class PannelloDati extends JPanel
{
	private JLabel lLivello;
	private JLabel lObiettivi;
	private JLabel lPunteggio;

	private JTextField livello;
	private JTextField obiettivi;
	private JTextField punteggio;
	public PannelloDati(int liv, int ob, int punt)
	{

		setLayout(new FlowLayout());
		lLivello = new JLabel("Livello");
		lObiettivi = new JLabel("Obiettivi");
		lPunteggio = new JLabel("Punteggio");

		livello = new JTextField(5);
		obiettivi = new JTextField(5);
		punteggio = new JTextField(5);

		livello.setText(String.format("%d",liv));
		obiettivi.setText(String.format("%d/10",ob));
		punteggio.setText(String.format("%d",punt));

		add(lLivello);
		add(livello);
		add(lObiettivi);
		add(obiettivi);
		add(lPunteggio);
		add(punteggio);

		livello.setEditable(false);
		obiettivi.setEditable(false);
		punteggio.setEditable(false);

		livello.setHorizontalAlignment(JTextField.CENTER);
		obiettivi.setHorizontalAlignment(JTextField.CENTER);
		punteggio.setHorizontalAlignment(JTextField.CENTER);

	}

	public void aggiornaLivello(int liv){ livello.setText(String.format("%d",liv));	}
	public void aggiornaObiettivi(int ob){ obiettivi.setText(String.format("%d/10",ob));}
	public void aggiornaPunteggio(int p){ punteggio.setText(String.format("%d",p));}

}