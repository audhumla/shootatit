public class Vettore
{
	//Campi privati: double x,y
	private double x,y, lunghezza;


	public Vettore(double miox, double mioy)
	{
		x = miox;
		y = mioy;
		lunghezza = length();

	}
	//Metodi set e get
	public void setX(double miox)
	{
		x = miox;
	}

	public void setY(double mioy)
	{
		y = mioy;
	}

	public double getX()
	{
		return x;
	}

	public double getY()
	{
		return y;
	}
	// Metodo "length()" che restituisce un double pari al valore della lunghezza del vettore

	public double length()
	{
		return lunghezza = Math.sqrt(x*x+y*y);

	}

	public void normalizza()
	{
		x=x/lunghezza;
		y=y/lunghezza;
	}

}
