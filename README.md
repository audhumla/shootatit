# README #

# Shoot at it #

This is a simple project made in 2013 for a college exam: "Java Programming and graphics management".
It is a very basic "Shoot At It" videogame. 
## Instructions ##

If you already installed java compiler and java, browse with your prompt or terminal in the directory with all project files, and just type:


```
#!bash

javac *.java
java ShootAtIt
```

