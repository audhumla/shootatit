import java.awt.geom.*;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

public class Cerchio extends Ellipse2D.Double
{

	Ellipse2D.Double cerchio;
	private double x,y,r; //x: ascisse del centro del cerchio, y: ordinata del centro del cerchio, r: raggio del cerchio
	Color color;


	public Cerchio( Point2D.Double centro, double raggio, boolean red )
	{

		x = centro.getX();
		y = centro.getY();
		r = raggio;

		if (red)
			color = new Color(255,0,0);
		else
			color = new Color(255,255,255);


		cerchio = new Ellipse2D.Double( x-raggio,  y-raggio, 2*raggio, 2*raggio );

	}

	public void setX(double X){ x = X; }
	public double getX(){return x;}

	public void getY(double Y){ y = Y;}
	public double getY(){return y;}

	public void setRaggio(double R){r = R;}
	public double getRaggio(){return r;}

	public void setPosizione(Point2D.Double centro)
	{
		x=centro.getX();
		y=centro.getY();
		cerchio.setFrame( x-r, y-r, 2*r, 2*r );
	}

	public void paint(Graphics2D g2)
	{
		g2.setColor(color);
		g2.fill(cerchio);

	}




}