import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import java.awt.Dimension;


class Bersaglio
{
	private Cerchio[] bersaglio;
	private Point2D.Double centro;

	private Vettore direzione;
	private double velocita;
	final private double r, height, width;

	public Bersaglio(double R, double v)
	{

		r = R; //R � il raggio del bersaglio, ossia del cerchio pi� grande
		velocita = v;
		bersaglio = new Cerchio[5];
		width = 784;
		height = 461;
		generaBersaglio();

		bersaglio[4] = new Cerchio(centro,r/5,true);
		bersaglio[3] = new Cerchio(centro,2*r/5,false);
		bersaglio[2] = new Cerchio(centro,3*r/5,true);
		bersaglio[1] = new Cerchio(centro,4*r/5,false);
		bersaglio[0] = new Cerchio(centro,r,true);


	}

	public int cerchioColpito(Point2D shoot)
	{
		double distanza = Math.sqrt((shoot.getX()- bersaglio[0].getX())*(shoot.getX()- bersaglio[0].getX()) + (shoot.getY()- bersaglio[0].getY())*(shoot.getY()- bersaglio[0].getY()));
		int n_cerchio;
		for (n_cerchio=4; n_cerchio>=0; n_cerchio--)
				if (distanza<=bersaglio[n_cerchio].getRaggio())
				break;
		return n_cerchio+1; //1<=cerchio<=5, il n.5 � quello pi� interno, il n. 1 quello pi� esterno, n_cerchio = 0 se non viene preso nessun cerchio

	}

 	public void disegna( Graphics2D g2 )
	{

		for(int i=0;i<=4;i++){
			bersaglio[i].paint(g2);
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		}
	}

	public void movimento()
	{

		centro = new Point2D.Double( centro.getX()+(direzione.getX()*velocita) , centro.getY()+(direzione.getY()*velocita)  );
		bersaglio[0].setPosizione(centro);
		bersaglio[1].setPosizione(centro);
		bersaglio[2].setPosizione(centro);
		bersaglio[3].setPosizione(centro);
		bersaglio[4].setPosizione(centro);


	}

	public void generaBersaglio() //generatore del centro e della direzione del bersaglio
	{
		int rand; // rand decide pseudo-randomicamente se il bersaglio deve essere generato sulla diagonale, a destra, a sinistra, in alto o in basso rispetto al pannello
		rand = randomInt(8,1);
		switch(rand)
		{
			case 1: //bersaglio generato sulla diagonale in alto a sinistra

				centro = new Point2D.Double(-r,-r*(height/width));
				direzione = new Vettore(width,height);
				direzione.normalizza();
				break;

			case 2:	//bersaglio generato in alto
				centro = new Point2D.Double(randomDouble(width-r,r),-r);
				direzione = new Vettore(0,1);
				break;

			case 3: //bersaglio generato sulla diagonale in alto a destra
				centro = new Point2D.Double(width+r, -r*(height/width));
				direzione = new Vettore(-width,height);
				direzione.normalizza();
				break;

			case 4:	//bersaglio generato a destra
				centro = new Point2D.Double(width+r,randomDouble(height-r,r));
				direzione = new Vettore(-1,0);
				break;

			case 5: //bersaglio generato sulla diagonale in basso a destra
				centro = new Point2D.Double(width+r,(width+r)*(height/width));
				direzione = new Vettore(-width,-height);
				direzione.normalizza();
				break;

			case 6: //bersaglio generato in basso
				centro = new Point2D.Double(randomDouble(width-r,r),height+r);
				direzione = new Vettore(0,-1);
				break;

			case 7: //bersaglio generato sulla diagonale in basso a sinistra
				centro = new Point2D.Double(-r,r*(height/width)+height);
				direzione = new Vettore(width,-height);
				direzione.normalizza();
				break;

			case 8: //bersaglio generato a sinistra
				centro = new Point2D.Double(-r,randomDouble(height-r,r));
				direzione = new Vettore(1,0);
				break;
		}

	}

	public static double randomDouble(double max,double min)
	{
		return Math.floor(Math.random()*(max-min+1)+min);
	}

	public static int randomInt(int max,int min)
	{
		return (int)Math.floor(Math.random()*(max-min+1)+min);
	}


	public Point2D getCentro(){return centro;}

	public double getX(){return centro.getX();}

	public double getY(){return centro.getY();}

	public void setVelocita(double v){velocita = v;}

	public double getVelocita(){return velocita;}


}